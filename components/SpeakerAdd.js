function SpeakerAdd({eventYear, insertRecord}) {
    return (
        <a href="#" className="addSes">
            <i onClick={e => {
                e.preventDefault();
                const firstLast = window.prompt("Enter first and last name:", "");
                const [first, last] = firstLast.split(' ');

                insertRecord({id: "9999", first, last, bio: "Bio not yet entered", sessions: [
                    {
                        id: "8888", title: "New session", room: { name: "Main room"}, eventYear
                    }
                ]})
            }}>+</i>
        </a>
    )
}

export default SpeakerAdd;
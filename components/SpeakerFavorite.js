import {useState, useContext} from 'react';
import {SpeakerContext} from '../contexts/SpeakerContext'

function SpeakerFavorite() {
    const [isUpdating, setIsUpdating] = useState(false);
    
    const {speaker, updateRecord} = useContext(SpeakerContext);

    function doneCallback() {
        setIsUpdating(false);
    }

return (
    <div className="action padB1">
        <span onClick={() => {
            setIsUpdating(true);
            updateRecord({...speaker, favorite: !speaker.favorite}, doneCallback)
            }}>
            <i className={speaker.favorite ? "fa fa-star orange" : "fa fa-star-o"} />{" "}Favorite{" "}
            {isUpdating ? <span className="fa fa-circle-notch fa-spin"></span>: null}
        </span>
    </div>
)
}

export default SpeakerFavorite;
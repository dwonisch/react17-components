import SpeakerFavorite from "./SpeakerFavorite";
import { SpeakerFilterContext} from "../contexts/SpeakerFilterContext";
import { SpeakerProvider, SpeakerContext } from "../contexts/SpeakerContext";
import { useContext, useState, memo } from "react";
import SpeakerDelete from './SpeakerDelete';
import ErrorBoundary from './ErrorBoundary';

function Session(props) {
    return (
    <span className="session w-100">
        {props.title} <strong>Room: {props.room}</strong>
    </span>        
    );
}

function Sessions() {
    const {eventYear} = useContext(SpeakerFilterContext);
    const {speaker} = useContext(SpeakerContext);

    return (
    <div className="sessionBox card h-250">
    {speaker.sessions.filter(s => s.eventYear === eventYear).map(s => {
        return (
        <Session key={s.id} title={s.title} room={s.room.name}></Session>
        );
    })}
</div>);
}

function ImageWithFallback({src, ...props}) {
    const [error, setError] = useState(false)
    const [imgSrc, setImgSrc] = useState(src)

    function onError(){
        if(!error) {
            setImgSrc("/images/speaker-99999.jpg");
            setError(true)
        }
    }

    return (
        <img src={imgSrc} {...props} onError={onError} />
    )
}

function SpeakerImage() {
    const { speaker: {id, first, last}} = useContext(SpeakerContext);

    return (                
    <div className="speaker-img d-flex flex-row justify-content-center align-items-center h-300">
        <ImageWithFallback className="contain-fit" src={`/images/speaker-${id}.jpg`} width="300" alt={`${first} ${last}`} />
    </div>);
}

function SpeakerDemographics() {
    const {speaker} = useContext(SpeakerContext);
    return (
        <div className="speaker-info">
        <div className="d-flex justify-content-between mb-3">
            <h3 className="text-truncate w-200">
                {speaker.first} {speaker.last}
            </h3>
        </div>
        <SpeakerFavorite></SpeakerFavorite>
        <div>
            <p className="card-description">{speaker.bio.substring(0,70)}</p>
            <div className="social d-flex flex-row mt-4">
                <div className="company">
                    <h5>Company</h5>
                    <h6>{speaker.company}</h6>
                </div>
                <div className="twitter">
                    <h5>Twitter</h5>
                    <h6>{speaker.twitterHandle}</h6>
                </div>
            </div>
        </div>
    </div>
    );
}

function Speaker(props){
    return (
        <ErrorBoundary showErrorCard={true} errorUI={ErrorSpeaker}>
            <SpeakerNoErrorBoundary {...props}></SpeakerNoErrorBoundary>
        </ErrorBoundary>
    )
}

function ErrorSpeaker(){

    if(showErrorCard) {
        return (
            <div className="col-xs-12 col-sm-12 col-sm-6 col-lg-4 col-sm-12 col-cs-12">
                <div className="card card-height p-4 m-4">
                    <img src="/speakers/speaker-99999.jpg" />
                    <bold>Error loading speaker</bold>
                </div>
            </div>
        )
    }
    return null;
}

const SpeakerNoErrorBoundary = memo(function ({speaker, updateRecord, insertRecord, deleteRecord, showErrorCard}) {
    const {showSessions} = useContext(SpeakerFilterContext);

    return (
        <SpeakerProvider speaker={speaker} updateRecord={updateRecord} insertRecord={insertRecord} deleteRecord={deleteRecord}>
            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <div className="card card-height p-4 mt-4">
                    <SpeakerImage></SpeakerImage>
                    <SpeakerDemographics></SpeakerDemographics>
                </div>
                {showSessions ? <Sessions></Sessions> : null}
                <SpeakerDelete></SpeakerDelete>
            </div>
        </SpeakerProvider>
    );
}, (prev, next) => prev.speaker.favorite == next.speaker.favorite);

export default Speaker;
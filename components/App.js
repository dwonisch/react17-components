import Header from "./Header";
import Speakers from "./Speakers";
import Layout from "./Layout";

const App = () => {

  return (
    <Layout startingTheme="light">
        <Header></Header>
        <Speakers></Speakers>
    </Layout>
  );
};

export default App;
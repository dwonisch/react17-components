import {useContext} from 'react'
import ReactPlaceHolder from 'react-placeholder'
import Speaker from "./Speaker";
import {SpeakerFilterContext} from '../contexts/SpeakerFilterContext';
import SpeakerAdd from './SpeakerAdd'


import { data } from "../SpeakerData";
import useRequestRest, {REQUEST_STATUS} from "../hooks/useRequestRest";

const SpeakersList = () => {
  const {data: speakersData, requestStatus, error, updateRecord, insertRecord, deleteRecord} = useRequestRest();
  
  const {showSessions, searchQuery, eventYear} = useContext(SpeakerFilterContext);

  if(requestStatus === REQUEST_STATUS.ERROR) {
    return (<div className="text-danger">ERROR: <b>Loading speaker data failed {error}</b></div>)
  }

  return (<div className="container speakers-list">
    <ReactPlaceHolder type="media" rows={15} className="speakerslist-placeholder" ready={requestStatus === REQUEST_STATUS.SUCCESS}>
      <SpeakerAdd eventYear={eventYear} insertRecord={insertRecord}></SpeakerAdd>
      <div className="row">
          {speakersData.filter(s => {
            return s.first.toLowerCase().includes(searchQuery.toLowerCase()) || s.last.toLowerCase().includes(searchQuery.toLowerCase())
          }).filter(s => {
            return s.sessions.find(session => session.eventYear === eventYear)
          }).map(speaker => { 
              return (
                  <Speaker key={speaker.id} speaker={speaker} showSessions={showSessions} updateRecord={updateRecord} insertRecord={insertRecord} deleteRecord={deleteRecord}></Speaker>
              );
          })}
      </div>
    </ReactPlaceHolder>
  </div>
  );
};

export default SpeakersList;
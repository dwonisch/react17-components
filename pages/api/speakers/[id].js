import path from "path";
import fs from "fs";

const {promisify} = require("util");
const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile)
const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));

export default async function handler(req, res) {
    const method = req?.method;
    const id = parseInt(req?.query.id);

    const recordFromBody = req?.body;

    async function putMethod() {    
        const jsonFile = path.resolve('./', "db.json");
        try{
            const readFileData = await readFile(jsonFile);
            await delay(1000);
            const speakers = JSON.parse(readFileData).speakers;
            if(!speakers) {
                res.status(404).send("Request failed with status 404")
            } else {
                const newSpeakers = speakers.map(s => s.id === recordFromBody.id ? recordFromBody : s)
                writeFile(jsonFile, JSON.stringify({speakers: newSpeakers}, null, 2))
                res.setHeader("Content-Type", "application/json");
                res.status(200).send(JSON.stringify(recordFromBody, null, 2))
            }
        } catch(e) {
            console.log(e)
        }
    }
    
    async function deleteMethod() {    
        const jsonFile = path.resolve('./', "db.json");
        try{
            const readFileData = await readFile(jsonFile);
            await delay(1000);
            const speakers = JSON.parse(readFileData).speakers;
            if(!speakers) {
                res.status(404).send("Request failed with status 404")
            } else {
                const newSpeakers = speakers.filter(s => s.id != recordFromBody.id)
                writeFile(jsonFile, JSON.stringify({speakers: newSpeakers}, null, 2))
                res.setHeader("Content-Type", "application/json");
                res.status(200).send(JSON.stringify(speakers.find(s => s.id === recordFromBody.id), null, 2))
            }
        } catch(e) {
            console.log(e)
        }
    }
    
    async function postMethod() {    
        const jsonFile = path.resolve('./', "db.json");
        try{
            const readFileData = await readFile(jsonFile);
            await delay(1000);
            const speakers = JSON.parse(readFileData).speakers;
            if(!speakers) {
                res.status(404).send("Request failed with status 404")
            } else {
                const newId = speakers.reduce((accumulator, currentValue) => {
                    const currentId = parseInt(currentValue.id);
                    return currentId > accumulator ? currentId : accumulator;
                }, 0) + 1;
                
                const newSpeakerRecord = {...recordFromBody, id: newId}
    
                const newSpeakers = [newSpeakerRecord, ...speakers];
                
                writeFile(jsonFile, JSON.stringify({speakers: newSpeakers}, null, 2))
                res.setHeader("Content-Type", "application/json");
                res.status(200).send(JSON.stringify(newSpeakerRecord, null, 2))
            }
        } catch(e) {
            console.log(e)
        }
    }

    switch(method) {
        default:
            res.status(501).send("Method not implemented");
        break;
        case "POST":
            await postMethod();
            break;
        case "PUT":
            await putMethod();
            break;
        case "DELETE":
            await deleteMethod();
            break;
    }
}
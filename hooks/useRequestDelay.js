import {useState, useEffect} from 'react';

export const REQUEST_STATUS = {
    LOADING: 'loading',
    SUCCESS: 'success',
    ERROR: 'error'
}

function useRequestDelay(delayMs = 1000, initialData = []) {
  const [data, setData] = useState(initialData) 
  const [requestStatus, setRequestStatus] = useState(REQUEST_STATUS.LOADING)
  const [error, setError] = useState("")

  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  useEffect(() => {

    async function delayFunc(){
      try {
        await delay(delayMs);
        //throw "Custom Error";
        setData(data);
        setRequestStatus(REQUEST_STATUS.SUCCESS);
      } catch(e) {
        setRequestStatus(REQUEST_STATUS.ERROR);
        setError(e);
      } 
    }

    delayFunc();
  }, []);

  function updateRecord(record, doneCallback) {
    const originalRecords = [...data]
    const newRecords = data.map(d => d.id === record.id ? record : d);

    async function delayFunction(){
        try{
            setData(newRecords);
            await delay(delayMs);
            if(doneCallback) {
              doneCallback();
            }
        } catch(e) {
            setRequestStatus(REQUEST_STATUS.ERROR)
            setError(e);
            setData(originalRecords)
        }
    }
    delayFunction();

  }

  function insertRecord(record, doneCallback) {
    const originalRecords = [...data]
    const newRecords = [...data, record]

    async function delayFunction(){
        try{
            setData(newRecords);
            await delay(delayMs);
            if(doneCallback) {
              doneCallback();
            }
        } catch(e) {
            setRequestStatus(REQUEST_STATUS.ERROR)
            setError(e);
            setData(originalRecords)
        }
    }
    delayFunction();

  }

  function deleteRecord(record, doneCallback) {
    const originalRecords = [...data]
    const newRecords = data.filter(d => d.id !== record.id);

    async function delayFunction(){
        try{
            setData(newRecords);
            await delay(delayMs);
            if(doneCallback) {
              doneCallback();
            }
        } catch(e) {
            setRequestStatus(REQUEST_STATUS.ERROR)
            setError(e);
            setData(originalRecords)
        }
    }
    delayFunction();

  }

  return {data, requestStatus, error, updateRecord, insertRecord, deleteRecord}
}

export default useRequestDelay;
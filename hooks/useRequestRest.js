import {useState, useEffect} from 'react';
import axios from 'axios';

const REST_URL = 'api/speakers'

export const REQUEST_STATUS = {
    LOADING: 'loading',
    SUCCESS: 'success',
    ERROR: 'error'
}

function useRequestRest() {
  const [data, setData] = useState([]) 
  const [requestStatus, setRequestStatus] = useState(REQUEST_STATUS.LOADING)
  const [error, setError] = useState("")

  const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

  useEffect(() => {

    async function delayFunc(){
      try {
        const result = await axios.get(REST_URL);
        setData(result.data);
        setRequestStatus(REQUEST_STATUS.SUCCESS);
      } catch(e) {
        setRequestStatus(REQUEST_STATUS.ERROR);
        setError(e);
      } 
    }

    delayFunc();
  }, []);

  function updateRecord(record, doneCallback) {
    const originalRecords = [...data]
    const newRecords = data.map(d => d.id === record.id ? record : d);

    async function delayFunction(){
        try{
            setData(newRecords);
            await axios.put(`${REST_URL}/${record.id}`, record)
            if(doneCallback) {
              doneCallback();
            }
        } catch(e) {
            setRequestStatus(REQUEST_STATUS.ERROR)
            setError(e);
            setData(originalRecords)
        }
    }
    delayFunction();

  }

  function insertRecord(record, doneCallback) {
    const originalRecords = [...data]
    const newRecords = [...data, record]

    async function delayFunction(){
        try{
            const newRecord = await axios.post(`${REST_URL}/99999`, record)
            setData(newRecords);
            
            if(doneCallback) {
              doneCallback();
            }
        } catch(e) {
            setRequestStatus(REQUEST_STATUS.ERROR)
            setError(e);
            setData(originalRecords)
        }
    }
    delayFunction();

  }

  function deleteRecord(record, doneCallback) {
    const originalRecords = [...data]
    const newRecords = data.filter(d => d.id !== record.id);

    async function delayFunction(){
        try{
            setData(newRecords);
            await axios.delete(`${REST_URL}/${record.id}`, record)
            if(doneCallback) {
              doneCallback();
            }
        } catch(e) {
            setRequestStatus(REQUEST_STATUS.ERROR)
            setError(e);
            setData(originalRecords)
        }
    }
    delayFunction();

  }

  return {data, requestStatus, error, updateRecord, insertRecord, deleteRecord}
}

export default useRequestRest;